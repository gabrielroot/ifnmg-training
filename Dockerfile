FROM python:3.6.8-alpine

RUN apk add --no-cache --update gcc git openssh postgresql-dev musl-dev bash libxslt-dev libxml2-dev

ARG DEPLOY_PATH='/opt/training'

RUN mkdir -p $DEPLOY_PATH

ADD training/ $DEPLOY_PATH/training
ADD migrations/ $DEPLOY_PATH/migrations
ADD manage.py $DEPLOY_PATH/manage.py
ADD wsgi.py $DEPLOY_PATH/wsgi.py
# ADD gunicorn_settings.py $DEPLOY_PATH/gunicorn_settings.py

ADD requirements.txt $DEPLOY_PATH/requirements.txt

WORKDIR $DEPLOY_PATH

RUN pip install -U pip setuptools
RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "-w", "3", "wsgi:application"]
