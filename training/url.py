from flask import Blueprint

from .views.test import home
from .views.author import author

v1 = Blueprint('api_v1', __name__, url_prefix='/api/v1')
v1.add_url_rule('/', view_func=home, methods=['GET'])
v1.add_url_rule('/author', view_func=author, methods=['GET', 'POST'])
v1.add_url_rule('/author/<int:pk>', view_func=author, methods=['DELETE'])
